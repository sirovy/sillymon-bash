#!/bin/sh
#
#  Exec by cron every 5 min and run everything ./checks/
#
#
. ./config
. ./lib.sh

for check in $CHECKS_PATH/*
do
  if [ -x $check ]; then
    echo "Run check $check" | tee >> $LOG
    simple_check $check &
  fi
done
