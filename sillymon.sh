#!/bin/sh
#
#  Exec by cron every 5 min and run everything ./checks/
#
#
. ./config
. ./lib.sh

case $1 in
"status"|"show")
  $CUR_DIR/statut.sh
  ;;
"cron"|"check_all")
  $CUR_DIR/cron.sh
  ;;
"ping-add")
  ln -s $BIN_PATH/check_ping $CHECKS_PATH/$2
  ;;
"ping-rm")
  rm $CHECKS_PATH/$2
  ;;
*)
  echo " Run $1 with one of these options:"
  echo "  stus        -- return state of last checks"
  echo "  cron        -- do all checks like cron"
  echo "  ping-add    -- add ping check"
  echo "  ping-rm     -- remove ping check"
  echo ""
  echo " For more info read README"
  ;;
esac

